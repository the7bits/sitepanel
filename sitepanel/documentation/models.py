# coding: utf-8
from __future__ import unicode_literals
from django.db import models
from django.conf import settings

from django.utils import six
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from mptt.models import MPTTModel, TreeForeignKey

@python_2_unicode_compatible
class Page(MPTTModel):
    title = models.CharField(_('Title'), max_length=255)    
    slug = models.SlugField(_('Slug'))
    parent = TreeForeignKey("self", verbose_name=_(u"Parent"), blank=True, null=True, related_name='children')
    author = models.ForeignKey(settings.AUTH_USER_MODEL)

    body = models.TextField(_('Content'), blank=True)

    is_published = models.BooleanField(_('Published'), default=True)
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    last_edited = models.DateTimeField(_('Last edited'), auto_now=True)

    class Meta:
        verbose_name = _('Page')
        verbose_name_plural = _('Pages')
        ordering = ('title',)

    def __str__(self):
        return six.text_type(self.title)

