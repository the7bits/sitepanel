# coding: utf-8
from django.conf.urls.defaults import patterns, url

from .views import PageView

urlpatterns = patterns('',
    url(r'^(?P<slug>.+)/$', PageView.as_view(), name='page_view'),
)