# coding: utf-8
from __future__ import unicode_literals
from django.contrib import admin
from django import forms
from django.conf import settings

from django.utils import six
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from suit_ckeditor.widgets import CKEditorWidget
from suit.widgets import LinkedSelect
from treeadmin.admin import TreeAdmin

from .models import Page

class PageForm(forms.ModelForm):
    class Meta:
        widgets = {
            'body': CKEditorWidget(editor_options={'startupFocus': True})
        }


class PageAdmin(TreeAdmin):
    list_display = ('title', 'slug', 'author', 'is_published', 'last_edited',)
    search_fields = ('title', 'slug')
    list_filter = ('author', 'is_published')
    prepopulated_fields = {"slug": ("title",)}
    form = PageForm


admin.site.register(Page, PageAdmin)
