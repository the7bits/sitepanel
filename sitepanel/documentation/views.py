# coding: utf-8
from django.views.generic import DetailView
from .models import Page

class PageView(DetailView):
    model = Page
    queryset = Page.objects.filter(is_published=True)


