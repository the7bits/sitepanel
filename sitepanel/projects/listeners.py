# coding: utf-8

from django.db.models.signals import post_save

from projects.models import Project
# from api import citrus
from .utils import create_project_request


def create_project(sender, instance, created, **kwargs):
    if created:
        create_project_request(instance)

        # TODO: write log of response

post_save.connect(create_project, sender=Project)
