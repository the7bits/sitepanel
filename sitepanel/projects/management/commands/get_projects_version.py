# coding: utf-8
from __future__ import unicode_literals
from django.core.management.base import BaseCommand
from projects.models import Project, Statistic
from django.conf import settings
from mail.utils import send_mail
import requests
from datetime import datetime


class Command(BaseCommand):
    help = 'Get projects version.'

    def handle(self, *args, **options):
        url = 'http://{domain}/sbits-plugins-api/get-sshop-version/\
?secret_key={secret_key}&project_name={slug}'
        errors = {}
        for project in Project.objects.exclude(server__name="localhost")\
                .filter(engine__slug='sshop', active=True):
            link = url.format(
                domain=project.domain,
                secret_key=project.secret_key,
                slug=project.slug)
            ssc, created = Statistic.objects.get_or_create(
                project=project)
            try:
                r = requests.get(link)

                elapsed_time = r.elapsed.total_seconds()
                current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                new_dict = dict(ssc.data)
                new_dict[current_time] = (elapsed_time, r.status_code)
                ssc.data = dict(new_dict)
                ssc.save()

                if r.status_code in [200, 503]:
                    data = r.json()
                    project.version = data['version']
                    project.down_time = 0
                else:
                    project.down_time += 1
                    if project.down_time == 1 or project.down_time % 30 == 0:
                        errors[project.domain] = r.status_code
                    project.version = 'Unknown version'
            except Exception, e:
                project.version = 'Unknown version. Error - %s' % str(e)
            project.save()

        if errors:
            send_mail(
                to_emails=getattr(settings, 'SEND_ERROR_EMAILS', []),
                from_email=getattr(settings, 'DEFAULT_FROM_EMAIL', ''),
                subject='Check it!',
                html_template='error_mail_message.html',
                text_template='error_mail_message.txt',
                data={
                    'errors': errors,
                },
            )
