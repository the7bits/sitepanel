# coding: utf-8
from __future__ import unicode_literals
from django.core.management.base import BaseCommand
from projects.models import Project
from django.conf import settings
from mail.utils import send_mail
import requests


class Command(BaseCommand):
    help = 'Check on freezing 1c tasks.'

    def handle(self, *args, **options):
        url = 'http://{domain}/sbits-plugins-api/check-tasks-freezing/\
?secret_key={secret_key}&project_name={slug}'
        for project in Project.objects.exclude(server__name="localhost")\
                .filter(engine__slug='sshop', active=True):
            link = url.format(
                domain=project.domain,
                secret_key=project.secret_key,
                slug=project.slug)
            try:
                r = requests.get(link)
                if r.status_code == 200:
                    data = r.json()
                    if data['result'] == 'yes':
                        send_error(
                            project.domain,
                            r.status_code,
                            'Check 1c tasks!'
                        )
            except:
                pass
            project.save()


def send_error(domain, status, error):
    send_mail(
        to_emails=settings.SEND_ERROR_EMAILS,
        from_email=settings.DEFAULT_FROM_EMAIL,
        subject='Check it(1C): %s' % domain,
        html_template='error_mail_message.html',
        text_template='error_mail_message.txt',
        data=
        {
            'domain': domain,
            'status': status,
            'error': error,
        }
    )
