# coding: utf-8

from __future__ import unicode_literals
from datetime import datetime, timedelta
import requests
import random
import string
import os
import re

from django import forms
# from django.contrib.sites.models import Site
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.conf import settings
from django.conf.urls.defaults import patterns, url
# from suit.admin import SortableModelAdmin
from suit_ckeditor.widgets import CKEditorWidget
from suit.widgets import LinkedSelect
import reversion

from repository.models import Extension
from projects.forms import (AddProjectForm,
                            EditProjectForm)
from projects.utils import extension_list_url
from projects.utils import get_project_statistic
from projects.utils import get_project_version
from projects.models import (ProjectType,
                             Engine,
                             Version,
                             Server,
                             Project,
                             Statistic)


chars = string.ascii_letters + string.digits
PROJECT_DIR = getattr(settings, 'PROJECT_DIR')


def get_result(message):
    r = re.compile(r'(\[.+?])')
    for x in r.findall(message):
        message = message.replace(x, '')
    message = message.replace(' out: ', '').replace('  ', ' ')
    return message


# ------ actions
def update(modeladmin, request, queryset):
    result = ''
    for project in queryset:
        data = {
            'secret_key': project.server.sign,
            'slug': project.slug,
            'engine_slug': project.engine.slug,
            'engine_link': project.engine.get_last_version_link(),
        }
        url = project.server.manager_url + 'api/update-project/'
        result = requests.post(url, data)
        print result.json()
        # if project.theme_repo:
        #     theme_name = project.theme_repo.split('/')[-1].split('.')[0]
        # else:
        #     theme_name = ''

        # server = project.server
        # result = get_result(os.popen('cd %s/.. && fab connect:"%s,%s,%s" update:"%s,%s,%s,%s,"' % (PROJECT_DIR, server.host_string, server.user, server.password,
        #             server.name, project.name, project.engine.name, theme_name)).read())
        # get_project_version(project.slug)

    modeladmin.message_user(request, _(u'Projects will be updated soon'))
    # if len(queryset) == 1 and result:
    #     return render_to_response('text_file.html', RequestContext(request, {'text': result,
    #         'hide': True}))
update.short_description = _(u'Update selected projects')

def update_theme(modeladmin, request, queryset):
    result = ''
    for project in queryset:
        server = project.server
        result = get_result(os.popen('cd %s/.. && fab connect:"%s,%s,%s" update_theme:"%s,%s,%s,%s"' % (PROJECT_DIR, server.host_string, server.user, server.password,
                server.name, project.name, project.engine.name, project.theme_repo.split('/')[-1].split('.')[0])).read())

    modeladmin.message_user(request, _(u'Themes updated'))
    if len(queryset) == 1 and result:
        return render_to_response('text_file.html', RequestContext(request, {'text': result,
            'hide': True}))

update_theme.short_description = _(u'Update theme (if exist) of selected projects')

def restart(modeladmin, request, queryset):
    for project in queryset:
        server = project.server
        os.system('cd %s/.. && fab connect:"%s,%s,%s" touch:"%s,%s,%s"' % (PROJECT_DIR, server.host_string, server.user, server.password,
                        server.name, project.name, project.engine.name))
restart.short_description = _(u'Restart selected projects')

def restart_server(modeladmin, request, queryset):
    for server in queryset:
        os.system('cd %s/.. && fab connect:"%s,%s,%s" restart_vm:"%s"' % (PROJECT_DIR, server.host_string, server.user, server.password,
                    server.name))
restart_server.short_description = _(u'Restart selected servers')

# ------ end actions


class ProjectTypeAdmin(reversion.VersionAdmin, admin.ModelAdmin):
    list_display = ('name', 'slug',)
    prepopulated_fields = {'slug': ('name',)}

admin.site.register(ProjectType, ProjectTypeAdmin)


class ServerForm(forms.ModelForm):
    class Meta:
        widgets = {
            'description': CKEditorWidget(editor_options={'startupFocus': True})
        }

class ServerAdmin(reversion.VersionAdmin, admin.ModelAdmin):
    form = ServerForm
    list_display = ('name', 'slug',)
    prepopulated_fields = {"slug": ("name",)}
    actions = [restart_server]

admin.site.register(Server, ServerAdmin)

# -------------------

class VersionInline(admin.StackedInline):
    model = Version
    suit_classes = 'suit-tab suit-tab-versions'

class EngineForm(forms.ModelForm):
    class Meta:
        widgets = {
            'description': CKEditorWidget(editor_options={'startupFocus': True}),
            'project_type': LinkedSelect,
        }

class StartProjectForm(forms.Form):
    domain = forms.CharField()
    project_name = forms.CharField()
    engine = forms.CharField()
    server = forms.ChoiceField(choices=[(i.id, i.name) for i in Server.objects.all()])
    theme_repo = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super(StartProjectForm, self).__init__(*args, **kwargs)
        self.fields['engine'].widget.attrs['readonly'] = True

class CronTasksForm(forms.Form):
    tasks = forms.CharField(widget=forms.Textarea)

class EngineAdmin(reversion.VersionAdmin, admin.ModelAdmin):
    form = EngineForm
    inlines = (VersionInline,)

    fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['project_type', 'name', 'slug', 'description', 'engine_repo', 'extra_dirs', 'extra_cmds'],
            }),
        (_('Service extensions'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['service_extensions'],
            })
    ]

    suit_form_tabs = (
        ('general', _(u'General')),
        ('versions', _(u'Versions')),
    )

    list_display = ('name', 'slug', 'project_type')
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ['name']
    ordering = ('name',)
    list_filter = ('project_type',)
    filter_horizontal = ('service_extensions',)
    actions = ['start_new_project', 'citrus_start_new_project']

    def start_new_project(self, request, queryset):
        if len(queryset) == 1:
            form = None
            if 'apply' in request.POST:
                form = StartProjectForm(request.POST)

                if form.is_valid():
                    data = form.cleaned_data
                    engine = Engine.objects.get(name=data['engine'])
                    domain = data['domain']
                    project_name = data['project_name']
                    server = Server.objects.get(pk=data['server'])
                    theme_repo = data['theme_repo']

                    secret_key = ''.join(random.choice(chars) for x in range(8))

                    result = os.popen('cd %s/.. && fab connect:"%s,%s,%s" start_project:"%s,%s,%s,%s,%s,%s,%s,%s"' % (PROJECT_DIR, server.host_string, server.user, server.password,
                        server.name, project_name, engine.engine_repo, theme_repo, '@'.join(engine.extra_dirs.split('\r\n')),
                        secret_key, domain, '@'.join(engine.extra_cmds.split('\r\n')))).read()

                    path_to_project = os.popen('cd %s/.. && fab connect:"%s,%s,%s" get_path_to_project:"%s,%s"' % (PROJECT_DIR, server.host_string, server.user, server.password,
                        server.name, project_name)).read()

                    path_to_logs = os.popen('cd %s/.. && fab connect:"%s,%s,%s" get_path_to_logs:"%s,%s"' % (PROJECT_DIR, server.host_string, server.user, server.password,
                        server.name, project_name)).read()

                    r = re.compile(r'\[RESULT\](.+?)\[RESULT\]')

                    # create Project
                    try:
                        Project.objects.create(name=project_name,engine=engine,slug=project_name,
                            server=server,description=domain,
                            secret_key=secret_key,theme_repo=theme_repo,domain=domain,
                            path_to_project=r.findall(path_to_project)[0], path_to_logs='\n'.join(r.findall(path_to_logs)))
                        get_project_version(project_name)
                    except Exception, e:
                        result += '\n%s' % e

                    self.message_user(request, "Проект создан")
                    return render_to_response('text_file.html', RequestContext(request, {'text': get_result(result),
                        'hide': True}))

            if not form:
                form = StartProjectForm(initial={'engine': queryset[0].name})

            return render(request, 'new_project.html', {'form': form, 'selected_action': request.POST['_selected_action']})
        else:
            pass

    start_new_project.short_description = _(u'Lime: OLD! Don\'t use it! Start selected project')

    def citrus_start_new_project(self, request, queryset):
        if len(queryset) == 1:
            form = None
            if 'apply' in request.POST:
                form = StartProjectForm(request.POST)

                if form.is_valid():

                    data = form.cleaned_data
                    engine = Engine.objects.get(name=data['engine'])
                    domain = data['domain']
                    project_name = data['project_name']
                    label = data['project_name']
                    server = Server.objects.get(pk=data['server'])

                    project_secret_code = ''.join(random.choice(chars) for x in range(8))

                    data = {
                        'label': label,
                        'name': project_name,
                        'domain': domain,
                        'description': 'project, tttest',
                        'engine': engine.name,
                        'project_secret_code': project_secret_code,
                    }


                    # create Project
                    Project.objects.create(
                        name=project_name,
                        engine=engine,
                        slug=project_name,
                        server=server,
                        description=domain,
                        secret_key=project_secret_code,
                        domain=domain,
                    )

                    # TODO: fix api_url (qiwi-1.the7bits.com'/'api/project/create/)
                    api_url = server.manager_url + 'api/project/create/'
                    res = requests.post(api_url, data=data)


                    # print api_url
                    # print data
                    # print res.content

                    text = res.content

                    self.message_user(request, _('Проект создается и вскоре будет доступным'))

                    return render_to_response(
                        'text_file.html',
                        RequestContext(request, {
                            # 'text': get_result(res),
                            'text': text,
                            'hide': True
                        })
                    )

            if not form:
                form = StartProjectForm(initial={'engine': queryset[0].name})

            return render(request, 'citrus_new_project.html', {
                'form': form,
                'selected_action': request.POST['_selected_action']
            })
        else:
            self.message_user(request, _(u'Choose only one engine'))

    citrus_start_new_project.short_description = _(u'Qiwi: Start selected project')

admin.site.register(Engine, EngineAdmin)

# -------------------


class ProjectAdmin(reversion.VersionAdmin, admin.ModelAdmin):

    add_form = AddProjectForm
    edit_form = EditProjectForm

    list_display = (
        'name', 'active', 'slug', 'engine', 'server', 'version', '_actions'
    )
    # edit_readonly_fields = ('slug', 'engine', 'server', 'secret_key', 'version')

    search_fields = ('name', 'slug')
    list_filter = ('engine', 'server')

    fieldsets_edit = [
        ('General', {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'name', 'slug', 'domain', 'active', 'engine',
                'server', 'secret_key', 'description',]
                # 'path_to_project', 'path_to_logs'],
        }),
        (_('Extensions'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['extensions'],
        }),
    ]

    filter_horizontal = ('extensions',)

    actions = [
        update,
        # restart,
        # 'add_cron_task', 'remove_cron_task'
    ]

    edit_suit_tabs = (
        ('general', _(u'General')),
        # ('info', _(u'Information')),
    )

    add_suit_form_includes = None
    edit_suit_form_includes = (
        ('project_information.html', 'top', 'info'),
    )

    def get_form(self, request, obj=None, **kwargs):
        """
        Use special form during project creation
        """
        defaults = {}
        if obj is None:
            defaults.update({
                'form': self.add_form,
            })
            self.fieldsets = None
            self.suit_form_tabs = None
            self.suit_form_includes = self.add_suit_form_includes
            self.readonly_fields = tuple()
        else:
            defaults.update({
                'form': self.edit_form,
            })
            self.fieldsets = self.fieldsets_edit
            self.suit_form_tabs = self.edit_suit_tabs
            self.suit_form_includes = self.edit_suit_form_includes
            # self.readonly_fields = self.edit_readonly_fields

        defaults.update(kwargs)

        return super(ProjectAdmin, self).get_form(request, obj, **defaults)

    def add_cron_task(self, request, queryset):
        if 'apply' in request.POST:
            tasks = request.POST['tasks']
            tasks = ';'.join(tasks.replace('\r', '').split('\n'))

            for project in queryset:
                server = project.server

                os.system(
                    'cd %s/.. && fab connect:"%s,%s,%s" add_cron_tasks:"%s,%s,%s,%s"' % (
                        PROJECT_DIR, server.host_string, server.user,
                        server.password, server.name, project.name,
                        project.engine.name, tasks))

            return HttpResponseRedirect(request.get_full_path())

        form = CronTasksForm()

        return render(
            request, 'cron_task.html',
            {
                'form': form, 'action_name': 'add_cron_task',
                'selected_action': request.POST['_selected_action'],
                'button_name': _(u'Add tasks')
            }
        )
    add_cron_task.short_description = _(u'Add cron tasks to selected projects')

    def remove_cron_task(self, request, queryset):
        if 'apply' in request.POST:
            tasks = request.POST['tasks']
            tasks = ';'.join(tasks.replace('\r', '').split('\n'))

            for project in queryset:
                server = project.server

                os.system(
                    'cd %s/.. && fab connect:"%s,%s,%s" remove_cron_tasks:"%s,%s,%s"' % (
                        PROJECT_DIR, server.host_string, server.user,
                        server.password, server.name, project.name, tasks)
                )

            return HttpResponseRedirect(request.get_full_path())

        form = CronTasksForm()

        return render(
            request, 'cron_task.html',
            {
                'form': form, 'action_name': 'remove_cron_task',
                'selected_action': request.POST['_selected_action'],
                'button_name': _(u'Remove tasks')
            }
        )
    remove_cron_task.short_description = _(u'Remove cron tasks in selected projects')

    def get_urls(self):
        urls = super(ProjectAdmin, self).get_urls()
        return urls + patterns(
            '',
            url(r'^get-current-plugins/(?P<project_id>\d*)$',
                self.admin_site.admin_view(self.get_current_plugins),
                ),
            url(r'^get-all-plugins/(?P<project_id>\d*)$',
                self.admin_site.admin_view(self.get_all_plugins),
                ),
            url(r'^project-statistic/(?P<project_id>\d*)$',
                self.admin_site.admin_view(self.project_statistic),
                ),
        )

    def get_current_plugins(self, request, project_id):
        """Return pip install for current plugins
        """
        result = _(u'Error. Please try again later.')
        project = Project.objects.get(id=project_id)
        link = extension_list_url.format(
            domain=project.domain,
            secret_key=project.secret_key,
            slug=project.slug)
        r = requests.get(link)
        if r.status_code == 200:
            urls = []
            data = r.json()
            versions = {}
            for x in data['extension_list']:
                if x['installed_version']:
                    versions[x['slug']] = x['installed_version']

            exts = Extension.objects.filter(slug__in=versions.keys())
            for ext in exts:
                try:
                    url = ext.versions.get(
                        version=versions[ext.slug]).file.url
                    urls.append(url)
                except:  # for testing on local machine
                    pass

            result = 'pip install %s' % ' '.join(urls)
        return render(
            request, 'pip_install.html',
            {
                'text': result,
                'button_text': _(u'Copy to clipboard')
            }
        )

    def get_all_plugins(self, request, project_id):
        """Return pip install for all plugins
        """
        result = _(u'Error. Please try again later.')
        project = Project.objects.get(id=project_id)
        urls = []
        for x in project.extensions.all():
            url = x.versions.all().order_by('-pk')[0].file.url
            urls.append(url)

        result = 'pip install %s' % ' '.join(urls)
        return render(
            request, 'pip_install.html',
            {
                'text': result,
                'button_text': _(u'Copy to clipboard')
            }
        )

    def project_statistic(self, request, project_id):
        return HttpResponse(get_project_statistic(request, project_id))

    def _actions(self, obj):
        html = '<div class="btn-group">\
        <a title="%s" class="btn" href="%s" target="blank">\
        <i class="icon-list"></i></a>' % (
            _(u'Current plugins'),
            'get-current-plugins/%s' % obj.id,
        )

        html += '<a title="%s" class="btn" href="%s" target="blank">\
        <i class="icon-th-list"></i></a>' % (
            _(u'All plugins'),
            'get-all-plugins/%s' % obj.id,
        )
        html += '</div>'
        return html
    _actions.allow_tags = True

admin.site.register(Project, ProjectAdmin)

admin.site.register(Statistic)
