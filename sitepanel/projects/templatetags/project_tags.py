# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from django import template
from django.conf import settings
from django.core.urlresolvers import reverse

from projects.models import Project
register = template.Library()

@register.inclusion_tag('project_info_block.html', takes_context=True)
def admin_project_info_block(context):
    try:
        object_id = context['object_id']
        project = Project.objects.get(pk=object_id)
        return {
            'path_to_project': project.path_to_project,
            'path_to_logs': project.path_to_logs.split('\n'),
            'project': project,
        }
    except:
        return {}



