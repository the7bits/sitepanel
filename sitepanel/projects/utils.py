# coding: utf-8
from projects.models import Project
import requests
from django.conf import settings
from mail.utils import send_mail
from django.utils.translation import ugettext_lazy as _
from datetime import datetime, timedelta
from django.template.loader import render_to_string
from django.template import RequestContext


project_version_url = 'http://{domain}/sbits-plugins-api/get-sshop-version/\
?secret_key={secret_key}&project_name={slug}'

extension_list_url = 'http://{domain}/sbits-plugins-api/get-extension-list/\
?secret_key={secret_key}&project_name={slug}'


def get_project_version(slug):
    project = Project.objects.get(slug=slug)
    link = project_version_url.format(
        domain=project.domain,
        secret_key=project.secret_key,
        slug=project.slug)
    try:
        r = requests.get(link)
        if r.status_code == 200:
            data = r.json()
            project.version = data['version']
        else:
            project.version = 'Unknown version'
            send_mail(
                to_emails=settings.SEND_ERROR_EMAILS,
                from_email=settings.DEFAULT_FROM_EMAIL,
                subject='Check it!',
                html_template='error_mail_message.html',
                text_template='error_mail_message.txt',
                data=
                {
                    'domain': project.domain,
                    'status': r.status_code,
                }
            )
    except:
        project.version = 'Unknown version'
    project.save()


def get_extension_version(ids, extension_name):
    projects = Project.objects.filter(id__in=ids)
    versions = {}
    for project in projects:
        try:
            link = extension_list_url.format(
                domain=project.domain,
                secret_key=project.secret_key,
                slug=project.slug)
            r = requests.get(link)
            if r.status_code == 200:
                data = r.json()
                versions[project.id] = 'Not installed'
                for ex in data['extension_list']:
                    if ex['slug'] == extension_name:
                        versions[project.id] = ex['installed_version']
            else:
                versions[project.id] =\
                    '<b>Unknown version - %s</b>' % r.status_code
        except Exception, e:
            if project.id in versions:
                versions[project.id] += '<b> - %s</b>' % e
            else:
                versions[project.id] = '<b>%s</b>' % str(e)
    return versions


def get_project_statistic(request, project_id):
    """Return project statistics
    """
    project = Project.objects.get(id=project_id)
    types = ['15min', '1hr', '4hr', '12hr', '1day', '7day', '1mo']
    info = {
        '15min':
        {
            'delta': 15*60,
            'format': '%H:%M',
            'title': _('Statistic for %s (last 15 minutes), page - %s'),
        },
        '1hr':
        {
            'delta': 60*60,
            'format': '%H:%M',
            'title': _('Statistic for %s (last hour), page - %s'),
        },
        '4hr':
        {
            'delta': 4*60*60,
            'format': '%H:%M',
            'title': _('Statistic for %s (last 4 hours), page - %s'),
        },
        '12hr':
        {
            'delta': 12*60*60,
            'format': '%#d %b %H:%M',
            'title': _('Statistic for %s (last 12 hours), page - %s'),
        },
        '1day':
        {
            'delta': 24*60*60,
            'format': '%#d %b %H:%M',
            'title': _('Statistic for %s (last day), page - %s'),
        },
        '7day':
        {
            'delta': 7*24*60*60,
            'format': '%#d %b %H:%M',
            'title': _('Statistic for %s (last week), page - %s'),
        },
    }
    show_type = '15min'
    if 'type=' in request.get_full_path().split('?')[-1]:
        _type = request.get_full_path().split('?')[-1].replace('type=', '')
        if _type in types:
            show_type = _type
    data_list = []
    error_list = []
    down_times = []
    try:
        data = project.statistic.data
        average = 0.
        min_delta = timedelta(seconds=100)
        delta = timedelta(seconds=info[show_type]['delta'])
        for key, value in sorted(data.items()):
            time = datetime.strptime(key, "%Y-%m-%d %H:%M:%S")
            if datetime.now() - time < delta:
                label = value[1] if value[1] != 200 else None
                average += value[0]
                data_list.append([str(key), value[0], str(value[1])])
                if label is not None:
                    if down_times:
                        if time - datetime.strptime(
                            down_times[-1].split(' - ')[-1],
                            "%Y-%m-%d %H:%M:%S"
                        ) < min_delta:
                            if ' - ' in down_times[-1]:
                                down_times[-1] =\
                                    down_times[-1].split(' - ')[0] +\
                                    ' - ' + str(key)
                            else:
                                down_times[-1] += ' - ' + str(key)
                        else:
                            down_times.append(str(key))
                    else:
                        down_times.append(str(key))
                    error_list.append([str(key), value[0], str(label)])
                elif down_times and ' - ' not in down_times[-1]:
                    down_times[-1] += ' - ' + str(key)
    except:
        pass
    return render_to_string(
        'statistic.html',
        RequestContext(
            request,
            {
                'id': project_id,
                'data': data_list,
                'error': error_list,
                'title': info[show_type]['title'] % (
                    project.name,
                    'http://%s/sbits-plugins-api/get-sshop-version/' %
                    project.domain
                ),
                'link': '/admin/projects/project/project-statistic/%s' %
                project_id,
                'format': info[show_type]['format'],
                'average_time': '%0.2f' % float(
                    average / len(data_list)) if data_list else '0',
                'down_times': down_times,
            }
        )
    )


def create_project_request(project):
    """
    Send request to create new project in remote server

    Args:
        * project - new project instance

    Return:
        requests object as result
    """
    try:
        data = {
            'secret_key': project.server.sign,
            'name': project.name,
            'slug': project.slug,
            'domain': project.domain,
            'engine_slug': project.engine.slug,
            'project_secret_code': project.secret_key,
            'engine_link': project.engine.get_last_version_link(),
            'extra': '@'.join(
                [a for a in project.engine.extra_cmds.replace(
                    '\r', ''
                ).split('\n')]),
        }
        url = project.server.manager_url + 'api/create-new-project/'

        result = requests.post(url, data)
        # print result.json()
        return result
    except:
        return None
