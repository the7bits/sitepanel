# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Version'
        db.create_table(u'projects_version', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('engine', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['projects.Engine'])),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('changelog', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'projects', ['Version'])

        # Adding model 'Server'
        db.create_table(u'projects_server', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('manager_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'projects', ['Server'])

        # Adding model 'Project'
        db.create_table(u'projects_project', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('engine', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['projects.Engine'])),
            ('server', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['projects.Server'])),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'projects', ['Project'])

        # Adding model 'Engine'
        db.create_table(u'projects_engine', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['projects.ProjectType'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'projects', ['Engine'])


    def backwards(self, orm):
        # Deleting model 'Version'
        db.delete_table(u'projects_version')

        # Deleting model 'Server'
        db.delete_table(u'projects_server')

        # Deleting model 'Project'
        db.delete_table(u'projects_project')

        # Deleting model 'Engine'
        db.delete_table(u'projects_engine')


    models = {
        u'projects.engine': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Engine'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'project_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.ProjectType']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.project': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Project'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'engine': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Engine']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Server']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.projecttype': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'ProjectType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.server': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Server'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.version': {
            'Meta': {'object_name': 'Version'},
            'changelog': ('django.db.models.fields.TextField', [], {}),
            'engine': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Engine']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['projects']