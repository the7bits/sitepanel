# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Project.version'
        db.add_column(u'projects_project', 'version',
                      self.gf('django.db.models.fields.CharField')(default=u'', max_length=100),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Project.version'
        db.delete_column(u'projects_project', 'version')


    models = {
        u'projects.engine': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Engine'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'engine_repo': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'extra_cmds': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'extra_dirs': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'project_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.ProjectType']"}),
            'service_extensions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['repository.Extension']", 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.project': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Project'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'engine': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Engine']"}),
            'extensions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['repository.Extension']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'path_to_logs': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'path_to_project': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'secret_key': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '100'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Server']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'}),
            'theme_repo': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '100'})
        },
        u'projects.projecttype': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'ProjectType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.server': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Server'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'host_string': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'user': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'projects.version': {
            'Meta': {'object_name': 'Version'},
            'changelog': ('django.db.models.fields.TextField', [], {}),
            'engine': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'versions'", 'to': u"orm['projects.Engine']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'repository.category': {
            'Meta': {'ordering': "(u'order',)", 'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'repository.extension': {
            'Meta': {'object_name': 'Extension'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['repository.Category']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_service_extension': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['projects']