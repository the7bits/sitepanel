# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'EnabledExtension'
        db.create_table(u'projects_enabledextension', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['projects.Project'])),
            ('extension', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['repository.Extension'])),
        ))
        db.send_create_signal(u'projects', ['EnabledExtension'])


    def backwards(self, orm):
        # Deleting model 'EnabledExtension'
        db.delete_table(u'projects_enabledextension')


    models = {
        u'projects.enabledextension': {
            'Meta': {'object_name': 'EnabledExtension'},
            'extension': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['repository.Extension']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Project']"})
        },
        u'projects.engine': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Engine'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'project_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.ProjectType']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.project': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Project'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'engine': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Engine']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Server']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.projecttype': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'ProjectType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.server': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Server'},
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.version': {
            'Meta': {'object_name': 'Version'},
            'changelog': ('django.db.models.fields.TextField', [], {}),
            'engine': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Engine']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'repository.category': {
            'Meta': {'ordering': "(u'order',)", 'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'repository.extension': {
            'Meta': {'object_name': 'Extension'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['repository.Category']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['projects']