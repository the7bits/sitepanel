# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Server.sign'
        db.add_column(u'projects_server', 'sign',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=20),
                      keep_default=False)


        # Changing field 'Project.slug'
        db.alter_column(u'projects_project', 'slug', self.gf('django.db.models.fields.CharField')(max_length=100))
        # Removing index on 'Project', fields ['slug']
        db.delete_index(u'projects_project', ['slug'])


    def backwards(self, orm):
        # Adding index on 'Project', fields ['slug']
        db.create_index(u'projects_project', ['slug'])

        # Deleting field 'Server.sign'
        db.delete_column(u'projects_server', 'sign')


        # Changing field 'Project.slug'
        db.alter_column(u'projects_project', 'slug', self.gf('django.db.models.fields.SlugField')(max_length=50))

    models = {
        u'projects.engine': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Engine'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'engine_repo': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'extra_cmds': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'extra_dirs': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'project_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.ProjectType']"}),
            'service_extensions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['repository.Extension']", 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.project': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Project'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'down_time': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'engine': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Engine']"}),
            'extensions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['repository.Extension']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'path_to_logs': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'path_to_project': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'secret_key': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '100'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Server']"}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'theme_repo': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'default': "u'Unknown version'", 'max_length': '100'})
        },
        u'projects.projecttype': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'ProjectType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'projects.server': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Server'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'host_string': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager_url': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'sign': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'user': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        u'projects.statistic': {
            'Meta': {'object_name': 'Statistic'},
            'data': ('jsonfield.fields.JSONField', [], {'default': '{}', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['projects.Project']", 'unique': 'True'})
        },
        u'projects.version': {
            'Meta': {'object_name': 'Version'},
            'changelog': ('django.db.models.fields.TextField', [], {}),
            'engine': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'versions'", 'to': u"orm['projects.Engine']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'repository.category': {
            'Meta': {'ordering': "(u'order',)", 'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'repository.extension': {
            'Meta': {'object_name': 'Extension'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['repository.Category']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_service_extension': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['projects']