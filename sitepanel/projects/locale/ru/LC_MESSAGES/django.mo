��    ,      |  ;   �      �  #   �  	   �  
   �                    *     /     6     H  
   P     [     j     |     �     �     �     �     �     �     �     �     �     �     �  &   �          ,     F  
   _     j     q     y     �     �     �     �     �  ,   �                    (  �  D  G        I     g     �     �     �     �     �  #   �     	     	  +   4	  -   `	     �	     �	     �	     �	  !   �	     �	     �	     
     
     +
     A
     [
  G   j
     �
  <   �
  <        H     d     q  '   �     �  2   �     �       2   (  Q   [     �     �     �      �     (   *                       +             ,      
       	                  "                 %               &             !              $                )   #                        '                   Add cron tasks to selected projects Add tasks Change log Current version Description Domain name Edit Engine Engine repository Engines Extensions Extra commands Extra directories File General Host string Information Manager API url Name Password Path to logs Project Project type Project types Projects Remove cron tasks in selected projects Remove tasks Restart selected projects Restart selected servers Secret key Server Servers Service extensions Slug Start selected project Theme repository Themes updated Update selected projects Update theme (if exist) of selected projects Username Version Versions example: user@45.98.114.718 Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-09-20 15:20+0300
PO-Revision-Date: 2013-04-12 07:41+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Добаить cron задачи к выбранным проектам Добавить задачи Лог изменений Текущая версия Описание Доменное имя Редактировать Движок репозиторий движка Движки Расширения Дополнительные команды Дополнительные каталоги Файл Базовые Хост строка Информация Адрес API менеджера Имя Пароль Путь к логам Проект Тип проекта Типы проектов Проекты Удалить cron задачи у выбранных проектах Удалить задачи Перезапустить выбранные проекты Перезапустить выбранные сервера Секретный ключ Сервер Сервера Сервисные расширения Ссылка Запустить выбранный проект Репозиторий темы Темы обновляются Обновить выбранные проекты Обновите тему (если есть) выбранных проектов Имя пользователя Версия Версии пример: user@45.98.114.718 