# coding: utf-8

from __future__ import unicode_literals
import re
import uuid

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.utils import six
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.exceptions import ValidationError

from repository.models import Extension
from jsonfield import JSONField


def get_file_path(instance, filename):
    return 'engines/%s/%s' % (uuid.uuid4(), filename)


def _slug_validator(value):
    """
    Validation slug

    Slug can contain lower chars, numbers and underscores.
    Slug can't start of number or underscores, only char.
    """

    pattern = '''
        # allowable symbols: lower char, numbers or underscores

        ^               # beginning of name
        [a-z]           # first char - only char, not number or underscores
        [a-z0-9_]*      # next symbols
        $               # end of string
    '''

    if not re.search(pattern, value, re.VERBOSE):
        raise ValidationError(_(u'%s is not correct server slug') % value)

    return


@python_2_unicode_compatible
class ProjectType(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    slug = models.SlugField(_('Slug'))

    class Meta:
        verbose_name = _('Project type')
        verbose_name_plural = _('Project types')
        ordering = ('name',)

    def __str__(self):
        return six.text_type(self.name)

@python_2_unicode_compatible
class Engine(models.Model):
    project_type = models.ForeignKey(ProjectType, verbose_name=_('Project type'))
    name = models.CharField(_('Name'), max_length=100)
    slug = models.SlugField(_('Slug'))
    description = models.TextField(_('Description'))
    engine_repo = models.CharField(_('Engine repository'), max_length=300)
    extra_dirs = models.TextField(_('Extra directories'), blank=True)
    extra_cmds = models.TextField(_('Extra commands'), blank=True)

    service_extensions = models.ManyToManyField(
        Extension,
        verbose_name=_('Service extensions'),
        null=True,
        blank=True,
        )

    class Meta:
        verbose_name = _('Engine')
        verbose_name_plural = _('Engines')
        ordering = ('name',)

    def __str__(self):
        return six.text_type(self.name)

    def get_last_version_link(self):
        max_version = max(
            [a['version'] for a in self.versions.values('version')]
        )
        version = self.versions.get(version=max_version)
        return version.file.url


@python_2_unicode_compatible
class Version(models.Model):
    engine = models.ForeignKey(Engine, verbose_name=_('Engine'), related_name='versions')
    version = models.CharField(_('Version'), max_length=100)
    file = models.FileField(_(u'File'), upload_to=get_file_path)
    changelog = models.TextField(_('Change log'))

    class Meta:
        verbose_name = _('Version')
        verbose_name_plural = _('Versions')

    def __str__(self):
        return six.text_type(self.version)

@python_2_unicode_compatible
class Server(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    slug = models.SlugField(_('Slug'), validators=[_slug_validator],
        help_text=_(u'Slug can contain lower chars, numbers and underscores. \
                      Slug can\'t start of number or underscores, only char.'))
    manager_url = models.URLField(_('Manager API url'))
    sign = models.CharField(_(u'Sign'), max_length=20)
    description = models.TextField(_('Description'))
    host_string = models.CharField(_('Host string'), max_length=100, blank=True, null=True, help_text=_(u'example: user@45.98.114.718'))
    user = models.CharField(_('Username'), blank=True, null=True, max_length=50)
    password = models.CharField(_('Password'), blank=True, null=True, max_length=50)

    class Meta:
        verbose_name = _('Server')
        verbose_name_plural = _('Servers')
        ordering = ('name',)

    def __str__(self):
        return six.text_type(self.name)

@python_2_unicode_compatible
class Project(models.Model):
    name = models.CharField(_('Name'), max_length=100, unique=True)
    slug = models.CharField(_('Slug'), max_length=100, validators=[_slug_validator],
        help_text=_(u'Slug can contain lower chars, numbers and underscores.\
                      Slug can\'t start of number or underscores, only char.'))
    engine = models.ForeignKey(Engine, verbose_name=_('Engine'))
    server = models.ForeignKey(Server, verbose_name=_('Server'))
    description = models.TextField(_('Description'))
    secret_key = models.CharField(_('Secret key'), max_length=100, default='')
    domain = models.CharField(_('Domain name'), max_length=100)

    # TODO: Deprecated field, need delete in future
    theme_repo = models.CharField(_('Theme repository'), max_length=300, blank=True)
    path_to_project = models.CharField(_('Path to project'), blank=True, max_length=200)
    path_to_logs = models.TextField(_('Path to logs'), blank=True)

    version = models.CharField(_('Current version'), default='Unknown version', max_length=100)
    active = models.BooleanField(_('Active'), default=True)
    down_time = models.PositiveSmallIntegerField(default=0)
    extensions = models.ManyToManyField(
        Extension,
        verbose_name=_('Extensions'),
        null=True,
        blank=True,
        )

    class Meta:
        verbose_name = _('Project')
        verbose_name_plural = _('Projects')
        ordering = ('name',)

    def __str__(self):
        return six.text_type(self.name)


class Statistic(models.Model):
    project = models.OneToOneField(Project, verbose_name=_(u'Project'))
    data = JSONField(_(u'Data'), default={}, blank=True)

    def __unicode__(self):
        return self.project.name
