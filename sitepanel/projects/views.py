# coding: utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
import os
import re
from base64 import b64encode
from projects.models import Server
from .utils import get_project_statistic
from django.http import HttpResponse
from django.utils import simplejson

from django.conf import settings
PROJECT_DIR = getattr(settings, 'PROJECT_DIR')
MEDIA_ROOT = getattr(settings, 'MEDIA_ROOT')

def get_file_by_path(request, server_id):
    # print request.POST
    if 'apply' in request.POST:
        text = '%s' % request.POST['text']

        text = b64encode(text).replace('=', ';')
        server = Server.objects.get(pk=server_id)
        os.system('cd %s/.. && fab connect:"%s,%s,%s" write_to_file:"%s,%s,%s"' % (PROJECT_DIR, server.host_string, server.user, server.password, server.name, request.GET['path'], text))
        return render_to_response('text_file.html', RequestContext(request, {}))
    else:
        server = Server.objects.get(pk=server_id)
        result = os.popen('cd %s/.. && fab connect:"%s,%s,%s" get_file:"%s,%s,%s"' % (PROJECT_DIR, server.host_string, server.user, server.password, server.name, request.GET['path'], MEDIA_ROOT)).read()
        r = re.compile(r'\[RESULT\](.+?)\[RESULT\]')
        try:
            result = r.findall(result)[0]
        except:
            result = ''
        if result:
            path = result
            result = open(result).read()
            os.system('rm %s' % path)
        return render_to_response('text_file.html', RequestContext(request, {'text': result.strip(), 'is_popup': True}))


def ajax_project_statistic(request, project_id):
    html = get_project_statistic(request, project_id)
    response = simplejson.dumps({'html': html})
    return HttpResponse(response)
