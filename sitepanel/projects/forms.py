# coding: utf-8

import random
import string

from django import forms
from suit_ckeditor.widgets import CKEditorWidget
from suit.widgets import LinkedSelect

from models import Project


def gen_sercret_key():
    chars = string.letters + string.digits
    return ''.join([random.SystemRandom().choice(chars) for i in range(10)])


class AddProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('name', 'slug', 'domain', 'active', 'engine', 'server', 'secret_key', 'description')
        widgets = {
            # 'description': CKEditorWidget(editor_options={'startupFocus': True}),
            'engine': LinkedSelect,
            'server': LinkedSelect,
        }

    def __init__(self, *args, **kwargs):
        super(AddProjectForm, self).__init__(*args, **kwargs)
        self.fields['secret_key'].initial = gen_sercret_key()


class EditProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        widgets = {
            # 'description': CKEditorWidget(editor_options={'startupFocus': True}),
            'engine': LinkedSelect,
            'server': LinkedSelect,
        }
