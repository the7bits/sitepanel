# coding: utf-8
from __future__ import unicode_literals

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
from suit.admin import SortableModelAdmin
from suit_ckeditor.widgets import CKEditorWidget
from suit.widgets import LinkedSelect
import reversion
from django.shortcuts import render
from projects.models import Project, Engine

from repository.models import Category, Extension, Version
import os
from django.conf import settings
from projects.utils import get_extension_version
import requests

PROJECT_DIR = getattr(settings, 'PROJECT_DIR')


class CategoryAdmin(reversion.VersionAdmin, SortableModelAdmin):
    list_display = ('name', 'slug',)
    prepopulated_fields = {"slug": ("name",)}
    sortable = 'order'

# --------------------


class VersionInline(admin.StackedInline):
    model = Version
    suit_classes = 'suit-tab suit-tab-versions'


class ExtensionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExtensionForm, self).__init__(*args, **kwargs)
        self.fields['extensions'].queryset = Extension.objects.exclude(
            slug=self.instance.slug)

    class Meta:
        widgets = {
            'description': CKEditorWidget(
                editor_options={'startupFocus': True}),
            'category': LinkedSelect,
            # 'extensions': MultipleChoiceField(
            #     widget=forms.CheckboxSelectMultiple)
        }


class UpdateProjects(forms.Form):
    extension = forms.CharField()
    versions = forms.ChoiceField()
    projects = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        queryset=Project.objects.all())

    def __init__(self, *args, **kwargs):
        super(UpdateProjects, self).__init__(*args, **kwargs)
        self.fields['extension'].widget.attrs['readonly'] = True
        if 'initial' in kwargs:
            if 'deleting' in kwargs['initial']:
                self.fields['versions'].widget = forms.HiddenInput()
            self.fields['versions'].choices = [
                (x.version, x.version) for x in kwargs['initial']['versions']
            ]
            self.fields['projects'].queryset =\
                kwargs['initial']['projects_queryset']
        else:
            versions = Version.objects.all().values('version')
            self.fields['versions'].choices = [
                (x['version'], x['version']) for x in versions
            ]


class ExtensionAdmin(reversion.VersionAdmin, admin.ModelAdmin):
    form = ExtensionForm
    inlines = (VersionInline,)

    fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'category', 'name', 'slug', 'description'
            ],
        }),
        (_('Meta information'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['is_service_extension', 'is_visible'],
        }),
        (_('Extensions'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['extensions'],
        }),
    ]

    filter_horizontal = ('extensions',)

    suit_form_tabs = (
        ('general', _('General')),
        ('versions', _('Versions')),
    )

    list_display = ('name', 'slug', 'category',
                    'is_service_extension', 'is_visible')
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ['name']
    ordering = ('name',)
    list_filter = ('category', 'is_service_extension', 'is_visible')

    actions = ['quick_install_extension',
               'quick_update_extension',
               'quick_remove_extension',
               ]

    def update_extension(self, request, queryset):
        if len(queryset) == 1:
            form = None
            if 'apply' in request.POST:
                form = UpdateProjects(request.POST)

                if form.is_valid():
                    version = form.cleaned_data['versions']
                    for project in form.cleaned_data['projects']:
                        server = project.server
                        os.system(
                            'cd %s/.. && fab connect:"%s,%s,%s" plugins:"%s,%s,%s,update_extension,%s,%s,%s"'
                            % (PROJECT_DIR,
                               server.host_string,
                               server.user,
                               server.password,
                               server.name,
                               project.name,
                               project.engine.name,
                               queryset[0].slug,
                               version,
                               request.user.username,))
                    self.message_user(request, "Расширения обновлено")
            else:
                if queryset[0].slug in \
                    [ex.slug for ex in Engine.objects.get(slug='sshop')
                        .service_extensions.all()]:
                    projects = Project.objects.exclude(
                        server__name="localhost")\
                        .filter(engine__slug='sshop',
                                active=True).values('id')
                else:
                    projects = Project.objects.filter(
                        extensions=queryset, active=True)\
                        .exclude(server__name="localhost")\
                        .filter(engine__slug='sshop').values('id')

                versions = get_extension_version(projects, queryset[0].slug)
                all_versions = queryset[0].versions.all().order_by('-pk')

                form = UpdateProjects(
                    initial={
                        'extension': '%s (%s)' %
                        (queryset[0].name, all_versions[0]),
                        'versions': all_versions,
                        'projects_queryset': Project.objects.filter(
                            id__in=projects),
                    })

                return render(
                    request,
                    'update_in_projects.html',
                    {
                        'form': form,
                        'projects_version': versions,
                        'selected_action':
                        request.POST['_selected_action']
                    })
        else:
            pass
    update_extension.short_description = _(u'Update selected extension on projects')

    def install_extension(self, request, queryset):
        if len(queryset) == 1:
            form = None
            if 'apply' in request.POST:
                form = UpdateProjects(request.POST)

                if form.is_valid():
                    version = form.cleaned_data['versions']
                    for project in form.cleaned_data['projects']:
                        server = project.server
                        os.system(
                            'cd %s/.. && fab connect:"%s,%s,%s" plugins:"%s,%s,%s,install_extension,%s,%s,%s"'
                            % (PROJECT_DIR,
                               server.host_string,
                               server.user,
                               server.password,
                               server.name,
                               project.name,
                               project.engine.name,
                               queryset[0].slug,
                               version,
                               request.user.username,))
                    self.message_user(request, "Расширения установлены")
            else:
                if queryset[0].slug in \
                    [ex.slug for ex in Engine.objects.get(slug='sshop')
                        .service_extensions.all()]:
                    projects = Project.objects.exclude(
                        server__name="localhost")\
                        .filter(engine__slug='sshop',
                                active=True).values('id')
                else:
                    projects = Project.objects.filter(
                        extensions=queryset, active=True)\
                        .exclude(server__name="localhost")\
                        .filter(engine__slug='sshop').values('id')

                versions = get_extension_version(projects, queryset[0].slug)
                all_versions = queryset[0].versions.all().order_by('-pk')

                form = UpdateProjects(
                    initial={
                        'extension': '%s (%s)' %
                        (queryset[0].name, all_versions[0]),
                        'versions': all_versions,
                        'projects_queryset': Project.objects.filter(
                            id__in=projects)
                    })

                return render(
                    request,
                    'install_in_projects.html',
                    {
                        'form': form,
                        'projects_version': versions,
                        'selected_action':
                        request.POST['_selected_action']
                    })
        else:
            pass
    install_extension.short_description = _(u'Install selected extension on projects')

    def remove_extension(self, request, queryset):
        if len(queryset) == 1:
            form = None
            if 'apply' in request.POST:
                form = UpdateProjects(request.POST)
                form.errors.pop('versions')
                if form.is_valid():
                    version = ''
                    for project in form.cleaned_data['projects']:
                        server = project.server
                        print server.host_string, server.user, server.password
                        os.system(
                            'cd %s/.. && fab connect:"%s,%s,%s" plugins:"%s,%s,%s,remove_extension,%s,%s,%s"'
                            % (PROJECT_DIR,
                               server.host_string,
                               server.user,
                               server.password,
                               server.name,
                               project.name,
                               project.engine.name,
                               queryset[0].slug,
                               version,
                               request.user.username,))
                    self.message_user(request, "Расширения обновлены")
            else:
                if queryset[0].slug in \
                    [ex.slug for ex in Engine.objects.get(slug='sshop')
                        .service_extensions.all()]:
                    projects = Project.objects.exclude(
                        server__name="localhost",)\
                        .filter(engine__slug='sshop',
                                active=True).values('id')
                else:
                    projects = Project.objects.filter(
                        extensions=queryset, active=True)\
                        .exclude(server__name="localhost")\
                        .filter(engine__slug='sshop').values('id')

                versions = get_extension_version(projects, queryset[0].slug)
                all_versions = queryset[0].versions.all().order_by('-pk')

                form = UpdateProjects(
                    initial={
                        'extension': '%s (%s)' %
                        (queryset[0].name, all_versions[0]),
                        'versions': all_versions,
                        'projects_queryset': Project.objects.filter(
                            id__in=projects),
                        'deleting': True,
                    })

                return render(
                    request,
                    'remove_in_projects.html',
                    {
                        'form': form,
                        'projects_version': versions,
                        'selected_action':
                        request.POST['_selected_action']
                    })
        else:
            pass
    remove_extension.short_description = _(u'Remove selected extension on projects')

    def quick_remove_extension(self, request, queryset):
        if len(queryset) == 1:
            form = None
            if 'apply' in request.POST:
                form = UpdateProjects(request.POST)
                form.errors.pop('versions')
                failed = []
                error = ''
                if form.is_valid():
                    for project in form.cleaned_data['projects']:
                        data = {
                            'project_name': project.slug,
                            'secret_key': project.secret_key,
                            'slug': queryset[0].slug,
                            'version': '',
                            'user': request.user.username,
                        }
                        url = 'http://%s/sbits-plugins-api/remove-extension/'
                        r = requests.post(url % project.domain, data=data)
                        try:
                            if eval(r.text)['result'] != 'success':
                                failed.append(project.slug)
                        except Exception, e:
                            failed.append('%s - %s' % (project.slug, str(e)))
                    if failed:
                        error = '. Возникли проблемы на: %s' % ';'.join(failed)
                    self.message_user(
                        request,
                        "Расширения удалены%s" % error)
            else:
                if queryset[0].slug in \
                    [ex.slug for ex in Engine.objects.get(slug='sshop')
                        .service_extensions.all()]:
                    projects = Project.objects.exclude(
                        server__name="localhost")\
                        .filter(engine__slug='sshop',
                                active=True).values('id')
                else:
                    projects = Project.objects.filter(
                        extensions=queryset, active=True,
                        engine__slug='sshop')\
                        .exclude(server__name="localhost")\
                        .values('id')

                versions = get_extension_version(projects, queryset[0].slug)
                all_versions = queryset[0].versions.all().order_by('-pk')

                form = UpdateProjects(
                    initial={
                        'extension': '%s (%s)' %
                        (queryset[0].name, all_versions[0]),
                        'versions': all_versions,
                        'projects_queryset': Project.objects.filter(
                            id__in=projects),
                        'deleting': True,
                    })

                return render(
                    request,
                    'quick_remove_in_projects.html',
                    {
                        'form': form,
                        'projects_version': versions,
                        'selected_action':
                        request.POST['_selected_action']
                    })
        else:
            pass
    quick_remove_extension.short_description = _(u'Quick remove selected extension on projects')

    def quick_install_extension(self, request, queryset):
        if len(queryset) == 1:
            form = None
            if 'apply' in request.POST:
                form = UpdateProjects(request.POST)
                failed = []
                error = ''
                if form.is_valid():
                    for project in form.cleaned_data['projects']:
                        data = {
                            'project_name': project.slug,
                            'secret_key': project.secret_key,
                            'slug': queryset[0].slug,
                            'version': form.cleaned_data['versions'],
                            'user': request.user.username,
                        }
                        url = 'http://%s/sbits-plugins-api/install-extension/'
                        r = requests.post(url % project.domain, data=data)
                        try:
                            if eval(r.text)['result'] != 'success':
                                failed.append(project.slug)
                        except Exception, e:
                            failed.append('%s - %s' % (project.slug, str(e)))
                    if failed:
                        error = '. Возникли проблемы на: %s' % ';'.join(failed)
                    self.message_user(
                        request,
                        "Расширения установлены%s" % error)
            else:
                if queryset[0].slug in \
                    [ex.slug for ex in Engine.objects.get(slug='sshop')
                        .service_extensions.all()]:
                    projects = Project.objects.exclude(
                        server__name="localhost")\
                        .filter(engine__slug='sshop',
                                active=True).values('id')
                else:
                    projects = Project.objects.filter(
                        extensions=queryset, active=True,
                        engine__slug='sshop')\
                        .exclude(server__name="localhost")\
                        .values('id')

                versions = get_extension_version(projects, queryset[0].slug)
                all_versions = queryset[0].versions.all().order_by('-pk')

                form = UpdateProjects(
                    initial={
                        'extension': '%s (%s)' %
                        (queryset[0].name, all_versions[0]),
                        'versions': all_versions,
                        'projects_queryset': Project.objects.filter(
                            id__in=projects)
                    })

                return render(
                    request,
                    'quick_install_in_projects.html',
                    {
                        'form': form,
                        'projects_version': versions,
                        'selected_action':
                        request.POST['_selected_action']
                    })
        else:
            pass
    quick_install_extension.short_description = _(u'Quick install selected extension on projects')

    def quick_update_extension(self, request, queryset):
        if len(queryset) == 1:
            form = None
            if 'apply' in request.POST:
                form = UpdateProjects(request.POST)
                failed = []
                error = ''
                if form.is_valid():
                    for project in form.cleaned_data['projects']:
                        data = {
                            'project_name': project.slug,
                            'secret_key': project.secret_key,
                            'slug': queryset[0].slug,
                            'version': form.cleaned_data['versions'],
                            'user': request.user.username,
                        }
                        url = 'http://%s/sbits-plugins-api/update-extension/'
                        r = requests.post(url % project.domain, data=data)
                        try:
                            if eval(r.text)['result'] != 'success':
                                failed.append(project.slug)
                        except Exception, e:
                            failed.append('%s - %s' % (project.slug, str(e)))
                    if failed:
                        error = '. Возникли проблемы на: %s' % ';'.join(failed)
                    self.message_user(
                        request,
                        "Расширения обновлены%s" % error)
            else:
                if queryset[0].slug in \
                    [ex.slug for ex in Engine.objects.get(slug='sshop')
                        .service_extensions.all()]:
                    projects = Project.objects.exclude(
                        server__name="localhost")\
                        .filter(engine__slug='sshop',
                                active=True).values('id')
                else:
                    projects = Project.objects.filter(
                        extensions=queryset, active=True,
                        engine__slug='sshop')\
                        .exclude(server__name="localhost")\
                        .values('id')

                versions = get_extension_version(projects, queryset[0].slug)
                all_versions = queryset[0].versions.all().order_by('-pk')

                form = UpdateProjects(
                    initial={
                        'extension': '%s (%s)' %
                        (queryset[0].name, all_versions[0]),
                        'versions': all_versions,
                        'projects_queryset': Project.objects.filter(
                            id__in=projects)
                    })

                return render(
                    request,
                    'quick_update_in_projects.html',
                    {
                        'form': form,
                        'projects_version': versions,
                        'selected_action':
                        request.POST['_selected_action']
                    })
        else:
            pass
    quick_update_extension.short_description = _(u'Quick update selected extension on projects')

admin.site.register(Category, CategoryAdmin)
admin.site.register(Extension, ExtensionAdmin)
