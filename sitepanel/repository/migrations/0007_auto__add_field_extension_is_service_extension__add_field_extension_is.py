# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Extension.is_service_extension'
        db.add_column(u'repository_extension', 'is_service_extension',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Extension.is_visible'
        db.add_column(u'repository_extension', 'is_visible',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Extension.is_service_extension'
        db.delete_column(u'repository_extension', 'is_service_extension')

        # Deleting field 'Extension.is_visible'
        db.delete_column(u'repository_extension', 'is_visible')


    models = {
        u'repository.category': {
            'Meta': {'ordering': "(u'order',)", 'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'repository.extension': {
            'Meta': {'object_name': 'Extension'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['repository.Category']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_service_extension': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'repository.version': {
            'Meta': {'object_name': 'Version'},
            'changelog': ('django.db.models.fields.TextField', [], {}),
            'extension': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'versions'", 'to': u"orm['repository.Extension']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['repository']