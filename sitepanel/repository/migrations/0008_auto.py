# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding M2M table for field extensions on 'Extension'
        db.create_table(u'repository_extension_extensions', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_extension', models.ForeignKey(orm[u'repository.extension'], null=False)),
            ('to_extension', models.ForeignKey(orm[u'repository.extension'], null=False))
        ))
        db.create_unique(u'repository_extension_extensions', ['from_extension_id', 'to_extension_id'])


    def backwards(self, orm):
        # Removing M2M table for field extensions on 'Extension'
        db.delete_table('repository_extension_extensions')


    models = {
        u'repository.category': {
            'Meta': {'ordering': "(u'order',)", 'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'repository.extension': {
            'Meta': {'object_name': 'Extension'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['repository.Category']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'extensions': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'extensions_rel_+'", 'to': u"orm['repository.Extension']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_service_extension': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'repository.version': {
            'Meta': {'object_name': 'Version'},
            'changelog': ('django.db.models.fields.TextField', [], {}),
            'extension': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'versions'", 'to': u"orm['repository.Extension']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['repository']