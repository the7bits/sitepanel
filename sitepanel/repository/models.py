# coding: utf-8
from __future__ import unicode_literals

import uuid

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.utils import six
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

def get_file_path(instance, filename):
    return 'repo/%s/%s' % (uuid.uuid4(), filename)

@python_2_unicode_compatible
class Category(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    slug = models.SlugField(_('Slug'))
    order = models.PositiveIntegerField(_('Order'), default=0)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ('order',)

    def __str__(self):
        return six.text_type(self.name)

@python_2_unicode_compatible
class Extension(models.Model):
    category = models.ForeignKey(Category, verbose_name=_('Category'))
    name = models.CharField(_('Name'), max_length=100)
    slug = models.SlugField(_('Slug'))
    description = models.TextField(_('Description'))
    extensions = models.ManyToManyField(
        'self', verbose_name=_('Extensions'),
        blank=True, null=True)

    is_service_extension = models.BooleanField(_('Service extension'), default=False)
    is_visible = models.BooleanField(_('Visible'), default=True)

    class Meta:
        verbose_name = _('Extension')
        verbose_name_plural = _('Extensions')

    def __str__(self):
        return six.text_type(self.category.name + ' - ' + self.name)

@python_2_unicode_compatible
class Version(models.Model):
    extension = models.ForeignKey(Extension, verbose_name=_('Extension'), related_name='versions')
    version = models.CharField(_('Version'), max_length=100)
    file = models.FileField(_(u'File'), upload_to=get_file_path)
    changelog = models.TextField(_('Change log'))

    class Meta:
        verbose_name = _('Version')
        verbose_name_plural = _('Versions')

    def __str__(self):
        return six.text_type(self.version)
