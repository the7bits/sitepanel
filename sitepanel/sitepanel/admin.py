# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext, ugettext_lazy as _
from django.core import urlresolvers
from django.contrib.admin.models import LogEntry

class LogEntryAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'user', 'toolbar')
    search_fields = ['object_repr']
    list_filter = ('content_type',)

    def toolbar(self, obj):
        return '''<a href="%s%s" class="btn" title="%s">
        <i class="icon-hand-right"></i></a>''' % (
            urlresolvers.reverse('admin:index'), 
            obj.get_admin_url(),
            _(u'Go to object'),
            )
    toolbar.short_description = _(u'Actions')
    toolbar.allow_tags = True

admin.site.register(LogEntry, LogEntryAdmin)