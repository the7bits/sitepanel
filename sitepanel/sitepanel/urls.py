# coding: utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.http import HttpResponse
from sitepanel.views import Index

admin.autodiscover()
from sitepanel.admin import *

urlpatterns = patterns(
    '',
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^robots\.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /", mimetype="text/plain")),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^api/', include('api.urls')),
    url(r'^pages/', include('documentation.urls')),
    url(r'^get-file-by-path/(?P<server_id>\d*)', 'projects.views.get_file_by_path', name='get_file_by_path'),
    url(r'^ajax-project-statistic/(?P<project_id>\d*)',
        'projects.views.ajax_project_statistic',
        name='ajax_project_statistic'),
    url(r'^ajax-custom-statistic/(?P<custom_id>\d*)',
        'statistic.views.ajax_custom_statistic',
        name='ajax_custom_statistic'),
    (r'^$', Index.as_view()),
)

# Load urls from extensions
import pkg_resources
for entrypoint in pkg_resources.iter_entry_points(group=settings.ENTRY_POINT_GROUP):
    if entrypoint.name == 'update_urls':
        update_urls = entrypoint.load()
        new_data = update_urls(globals())
        for i in new_data.keys():
            globals()[i] = new_data[i]
    elif entrypoint.name == 'install':
        module_name = entrypoint.module_name
        from sbits_plugins.models import Extension, Repository
        try:
            ext = Extension.objects.get(slug=module_name)
            if not ext.is_initialized:
                install_func = entrypoint.load()
                try:
                    install_func(settings.PROJECT_DIR)
                    ext.is_initialized = True
                except:
                    pass
            ext.is_installed = True
            ext.save()
        except Extension.DoesNotExist:
            ext = Extension.objects.create(
                repository=Repository.objects.all()[0],
                name=module_name,
                slug=module_name, 
                is_installed=True,
                installed_version=entrypoint.dist.version,
                latest_version=entrypoint.dist.version,
                downloadable_links=[{
                    'version':entrypoint.dist.version,
                    'file':'',
                    'changelog':'',
                    },])
    elif entrypoint.name == 'upgrade':
        module_name = entrypoint.module_name
        from sbits_plugins.models import Extension
        try:
            ext = Extension.objects.get(slug=module_name)
            if ext.need_run_update:
                update_func = entrypoint.load()
                try:
                    update_func(
                        ext.old_version, 
                        ext.installed_version,
                        settings.PROJECT_DIR)
                    ext.need_run_update = False
                    ext.save()
                except:
                    pass
        except Extension.DoesNotExist:
            pass


if getattr(settings, 'DEBUG', False):
    urlpatterns += patterns(
        'django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)', 'serve'),
    )
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)', 'django.views.static.serve', {
            'document_root': getattr(settings, 'MEDIA_ROOT', ''),
            }),
    )
