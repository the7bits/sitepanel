# coding: utf-8
from django.conf import settings

def main(request):
    """Main context processor
    """
    return {
        "DOMAIN": getattr(settings, 'DEFAULT_DOMAIN', ''),
        "PROJECT_NAME": getattr(settings, 'PROJECT_NAME', ''),
    }
