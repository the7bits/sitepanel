# -*- coding: utf-8 -*-
from fabric.api import *
from fabric.contrib.files import upload_template, append, exists
import random, string, re

def connect(_host, _user, _password):
    env.host_string = _host
    env.user = _user
    env.password = _password

def start_project(server, project_name, engine_repo, theme_repo, extra_dirs, project_secret_code, domain, extra_cmds):
    set_redirect(server, domain)
    engine_name = engine_repo.split('/')[-1].split('.')[0]
    theme_name = ''
    if theme_repo:
        theme_name = theme_repo.split('/')[-1].split('.')[0]
    params = '%s;%s;%s;%s;%s;%s;%s;%s;%s' % (project_name, engine_name, engine_repo, theme_name, theme_repo, extra_dirs, project_secret_code, domain, extra_cmds)
    run('fab start_project:"%s,%s"' % (server, params))

def remove_project(server, project_name):
    run('fab remove_project:"%s,%s"' % (server, project_name))

def set_redirect(server, domain):
    run('fab set_redirect:"%s,%s"' % (server, domain))

def get_file(server, path, folder='/tmp'):
    x = run('fab show_file:"%s,%s"' % (server, path))
    r = re.compile(r'\[PATH\](.+?)\[PATH\]')
    try:
        result = r.findall(x)[0]
        path = folder + '/' + result.split('/')[-1]
        get(result, path)
        print '[RESULT]%s[RESULT]' % path
    except:
        pass

def write_to_file(server, path, text):
    run("""fab write_to_file:'%s,%s,%s'""" % (server, path, text))

def update(server, project_name, engine_name, theme_name, branch=''):
    run('fab update:"%s,%s,%s,%s,%s"' % (server, project_name, engine_name, theme_name, branch))

def touch(server, project_name, engine_name):
    run('fab touch:"%s,%s,%s"' % (server, project_name, engine_name))


def plugins(server, project_name, engine_name, command, extension,
            version, username):
    run('fab plugins:"%s,%s,%s,%s,%s,%s,%s"' % (
        server, project_name, engine_name, command, extension,
        version, username))

def update_theme(server, project_name, engine_name, theme_name):
    run('fab update_theme:"%s,%s,%s,%s"' % (server, project_name, engine_name, theme_name))

def restart_vm(server):
    run('fab restart_vm:"%s"' % server)

def get_path_to_project(server, project_name):
    run('fab get_path_to_project:"vm3,lolshop"')

def get_path_to_logs(server, project_name):
    run('fab get_path_to_logs:"vm3,lolshop"')

def add_cron_tasks(server, project_name, engine_name, tasks):
    run('fab cron:"%s,%s,%s,%s"' % (server, project_name, engine_name, tasks))

def remove_cron_tasks(server, project_name, tasks):
    run('fab remove_cron_tasks:"%s,%s,%s"' % (server, project_name, tasks))