# coding: utf-8
from __future__ import unicode_literals

from django.utils.translation import ugettext as _
from django.views.generic.edit import CreateView, FormView
from django.utils.decorators import method_decorator
from django.db import models
from django.conf import settings
from django.utils import timezone
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login

from accounts.forms import InviteCreationForm, AccountCreationForm
from accounts.models import Account, Invite, ALREADY_ACTIVATED
from accounts.utils import get_random_string
from mail.utils import send_mail


class InviteCreate(CreateView):
    form_class = InviteCreationForm
    template_name = 'accounts/invite_new.html'
    success_url = 'success'

class AccountRegister(FormView):
    form_class = AccountCreationForm
    template_name = 'accounts/account_register.html'
    success_url = 'success'

    def form_valid(self, form):
        nickname = form.cleaned_data['nickname']
        activation_code = get_random_string()
        account = Account.objects.create_user(
                email=form.cleaned_data['email'],
                nickname=nickname,
                password=form.cleaned_data['password1']
                )
        account.is_active = False
        account.activation_code = activation_code
        account.save()

        invite = Invite.objects.get(is_active=True, code=self.request.GET['invite'])
        invite.is_active = False
        invite.date_used = timezone.now()
        invite.registered_account = account
        invite.save(update_fields=['is_active', 'date_used', 'registered_account'])

        send_mail(to_emails=[account.email],
                from_email=settings.DEFAULT_FROM_EMAIL,
                subject=_('Hello'),
                html_template='accounts/register_mail_message.html',
                text_template='accounts/register_mail_message.txt',
                data={
                        'domain': getattr(settings, 'SITE_DOMAIN', ''), 
                        'nickname': nickname,
                        'activation_code': activation_code,
                        "DOMAIN": getattr(settings, 'DEFAULT_DOMAIN', ''),
                        "PROJECT_NAME": getattr(settings, 'PROJECT_NAME', ''),                        
                    })
        return super(AccountRegister, self).form_valid(form)

def account_activate(request, nickname):
    try:
        account = Account.objects.get(nickname=nickname, is_active=False)
        code = request.GET.get('code', '')
        if account.activation_code == code:
            account.activation_code = ALREADY_ACTIVATED
            account.is_active = True
            account.save(update_fields=['activation_code', 'is_active'])
            return redirect('/accounts/login/')
        else:
            return render(request, 'accounts/activation_error.html')    
    except Account.DoesNotExist:
        return render(request, 'accounts/activation_error.html')


