Feature: User login
    In order to authenticate the given user
    As a user
    I want to use my email and password for logging in

    Scenario: Log in through the form at the main site
        Given I visit the django url "/accounts/login/"
        When I fill in the field named "username" with "admin@testmail.com"
        And I fill in the field named "password" with "123456"
        And I click on the button named "submit"
        Then I should see that the element with id "nickname-info" contains exactly "admin"

