Feature: Request invite
    In order to request the invite
    As a user
    I want to press the button and type data for requesting the invite

    Scenario Outline: Open the request invite page as anonymous user
        Given I visit the django url "/accounts/request-invite/"
        When I fill in the field named "email" with "<email>"
        And I click on the button named "submit"
        And I look around
        Then I should see the element with the css selector ".alert-success"
        And I should see an email is sent to "<email>" with subject "Your request is added to queue"

    Examples:
        | email                           |
        | test@testmail.com               |
        | john.doe@gmail.com              |
        | bla.bla.bla@test.co.uk          |

    Scenario Outline: Open the request invite page as anonymous user
        Given I visit the django url "/accounts/request-invite/"
        When I fill in the field named "email" with "<email>"
        And I click on the button named "submit"
        And I look around
        Then I should see the element with the css selector ".error"

    Examples:
        | email                           |
        | tttt                            |
        | john@doe@gmail.com              |
        |                                 |

    Scenario: Open the request invite page as authorized user
        Given I log in as user "admin@testmail.com" with password "123456"
        When I visit the django url "/accounts/request-invite/"
        Then I should see the element with the css selector ".alert-warning"

