# coding: utf-8
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
import reversion
from django.http import HttpResponse

from django.conf import settings
PROJECT_DIR = getattr(settings, 'PROJECT_DIR')


from django.conf.urls.defaults import patterns, url

from .utils import get_custom_statistic
from .models import CustomStatistic


class CustomStatisticAdmin(reversion.VersionAdmin, admin.ModelAdmin):
    list_display = (
        'url', '_actions'
    )

    def get_urls(self):
        urls = super(CustomStatisticAdmin, self).get_urls()
        return urls + patterns(
            '',
            url(r'^custom-statistic/(?P<statistic_id>\d*)$',
                self.admin_site.admin_view(self.custom_statistic),
                ),
        )

    def custom_statistic(self, request, statistic_id):
        return HttpResponse(get_custom_statistic(request, statistic_id))

    def _actions(self, obj):
        html = '<div class="btn-group">\
        <a title="%s" class="btn" href="%s" target="blank">\
        <i class="icon-time"></i></a>' % (
            _(u'Statistic'),
            'custom-statistic/%s' % obj.id,
        )
        html += '</div>'
        return html
    _actions.allow_tags = True

admin.site.register(CustomStatistic, CustomStatisticAdmin)
