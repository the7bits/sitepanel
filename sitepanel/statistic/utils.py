from .models import CustomStatistic
from django.utils.translation import ugettext_lazy as _
from datetime import datetime, timedelta
from django.template.loader import render_to_string
from django.template import RequestContext


def get_custom_statistic(request, custom_id):
    """Return custom statistics
    """
    custom = CustomStatistic.objects.get(id=custom_id)
    types = ['15min', '1hr', '4hr', '12hr', '1day', '7day', '1mo']
    info = {
        '15min':
        {
            'delta': 15*60,
            'format': '%H:%M',
            'title': _('Statistic for last 15 minutes, page - %s'),
        },
        '1hr':
        {
            'delta': 60*60,
            'format': '%H:%M',
            'title': _('Statistic for last hour, page - %s'),
        },
        '4hr':
        {
            'delta': 4*60*60,
            'format': '%H:%M',
            'title': _('Statistic for last 4 hours, page - %s'),
        },
        '12hr':
        {
            'delta': 12*60*60,
            'format': '%#d %b %H:%M',
            'title': _('Statistic for last 12 hours, page - %s'),
        },
        '1day':
        {
            'delta': 24*60*60,
            'format': '%#d %b %H:%M',
            'title': _('Statistic for last day, page - %s'),
        },
        '7day':
        {
            'delta': 7*24*60*60,
            'format': '%#d %b %H:%M',
            'title': _('Statistic for last week, page - %s'),
        },
    }
    show_type = '15min'
    if 'type=' in request.get_full_path().split('?')[-1]:
        _type = request.get_full_path().split('?')[-1].replace('type=', '')
        if _type in types:
            show_type = _type
    data_list = []
    error_list = []
    down_times = []
    try:
        data = custom.data
        average = 0.
        min_delta = timedelta(seconds=100)
        delta = timedelta(seconds=info[show_type]['delta'])
        for key, value in sorted(data.items()):
            time = datetime.strptime(key, "%Y-%m-%d %H:%M:%S")
            if datetime.now() - time < delta:
                label = value[1] if value[1] != 200 else None
                average += value[0]
                data_list.append([str(key), value[0], str(value[1])])
                if label is not None:
                    if down_times:
                        if time - datetime.strptime(
                            down_times[-1].split(' - ')[-1],
                            "%Y-%m-%d %H:%M:%S"
                        ) < min_delta:
                            if ' - ' in down_times[-1]:
                                down_times[-1] =\
                                    down_times[-1].split(' - ')[0] +\
                                    ' - ' + str(key)
                            else:
                                down_times[-1] += ' - ' + str(key)
                        else:
                            down_times.append(str(key))
                    else:
                        down_times.append(str(key))
                    error_list.append([str(key), value[0], str(label)])
                elif down_times and ' - ' not in down_times[-1]:
                    down_times[-1] += ' - ' + str(key)
    except:
        pass
    return render_to_string(
        'custom_statistic.html',
        RequestContext(
            request,
            {
                'id': custom_id,
                'data': data_list,
                'error': error_list,
                'title': info[show_type]['title'] % (
                    custom.url
                ),
                'link': '/admin/statistic/customstatistic/custom-statistic/%s' %
                custom_id,
                'format': info[show_type]['format'],
                'average_time': '%0.2f' % float(
                    average / len(data_list)) if data_list else '0',
                'down_times': down_times,
            }
        )
    )
