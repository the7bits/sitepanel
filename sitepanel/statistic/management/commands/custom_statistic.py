# coding: utf-8
from __future__ import unicode_literals
from django.core.management.base import BaseCommand
from statistic.models import CustomStatistic
import requests
from datetime import datetime


class Command(BaseCommand):
    help = 'Get custom statistic.'

    def handle(self, *args, **options):
        for ssc in CustomStatistic.objects.all():
            link = ssc.url
            try:
                r = requests.get(link)

                elapsed_time = r.elapsed.total_seconds()
                current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                new_dict = dict(ssc.data)
                new_dict[current_time] = (elapsed_time, r.status_code)
                ssc.data = dict(new_dict)
                ssc.save()
            except:
                # TODO: add logging
                pass
