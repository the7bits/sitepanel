from django.http import HttpResponse
from django.utils import simplejson
from .utils import get_custom_statistic


def ajax_custom_statistic(request, custom_id):
    html = get_custom_statistic(request, custom_id)
    response = simplejson.dumps({'html': html})
    return HttpResponse(response)
