# coding: utf-8
from django.conf.urls.defaults import patterns, url
# from django.contrib.auth.views import login, logout
# from django.views.generic import TemplateView

# from api.views import ProjectList, ProjectDetail, \
#     ServerList, ServerDetail, EngineList, EngineDetail, \
#     ProjectTypeList, ProjectTypeDetail, \
#     EngineVersionList, EngineVersionDetail, \
#     ExtensionList, ExtensionDetail, CategoryList, CategoryDetail, \
#     ExtensionVersionList, ExtensionVersionDetail

from api.views import (ExtensionList, ExtensionDetail, CategoryList,
                       CategoryDetail, ExtensionVersionList,
                       ExtensionVersionDetail)

urlpatterns = patterns(
    'api.views',
    url(r'^$', 'api_root'),
    # url(r'^projects/$', ProjectList.as_view(), name='project-list'),
    # url(r'^projects/(?P<pk>\d+)/$', ProjectDetail.as_view(), name='project-detail'),
    # url(r'^servers/$', ServerList.as_view(), name='server-list'),
    # url(r'^servers/(?P<pk>\d+)/$', ServerDetail.as_view(), name='server-detail'),
    # url(r'^engines/$', EngineList.as_view(), name='engine-list'),
    # url(r'^engines/(?P<pk>\d+)/$', EngineDetail.as_view(), name='engine-detail'),
    # url(r'^project-types/$', ProjectTypeList.as_view(), name='projecttype-list'),
    # url(r'^project-types/(?P<pk>\d+)/$', ProjectTypeDetail.as_view(), name='projecttype-detail'),
    # url(r'^versions/$', EngineVersionList.as_view(), name='version-list'),
    # url(r'^versions/(?P<pk>\d+)/$', EngineVersionDetail.as_view(), name='version-detail'),    

    url(r'^extensions/$', ExtensionList.as_view(), name='extension-list'),
    url(r'^extensions/(?P<pk>\d+)/$',
        ExtensionDetail.as_view(), name='extension-detail'),
    url(r'^categories/$', CategoryList.as_view(), name='category-list'),
    url(r'^categories/(?P<pk>\d+)/$',
        CategoryDetail.as_view(), name='category-detail'),
    url(r'^versions/$', ExtensionVersionList.as_view(), name='version-list'),
    url(r'^versions/(?P<pk>\d+)/$',
        ExtensionVersionDetail.as_view(), name='version-detail'),
    url(r'^update-engine/$', 'update_engine', name='update-engine'),
    url(r'^update-plugin/$', 'update_plugin', name='update-plugin'),
    url(
        r'^update-plugin-on-projects/$',
        'update_plugin_on_projects',
        name='update-plugin-on-projects'),
    url(
        r'^get-project-values/$',
        'get_project_values',
        name='get-project-values'),
    url(
        r'^get-project-env/$',
        'get_project_env',
        name='get-project-env'),
)
