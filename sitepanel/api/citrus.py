# coding: utf-8

import requests


def create_project_request(project):
    """
    Send request to create new project in remote server

    Args:
        * project - new project instance

    Return:
        requests object as result
    """

    data = {
        'sign': project.server.sign,
        'label': project.name,
        'name': project.slug,
        'domain': project.domain,
        'description': project.description,
        'engine': project.engine.slug,
        'project_secret_code': project.secret_key,
    }

    # TODO: fix it temporary hack. maybe write method in Server model
    if project.server.manager_url[-1] == '/':
        url = project.server.manager_url + 'api/project/create/'
    else:
        url = project.server.manager_url + '/api/project/create/'

    print url
    print data

    return requests.post(url, data)
