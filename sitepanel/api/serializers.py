# coding: utf-8
from __future__ import unicode_literals
from rest_framework import serializers
# from django.conf import settings

from projects.models import Project, Engine, Server, \
    ProjectType, Version as EngineVersion
from repository.models import Category, Extension, Version as ExtensionVersion


class EngineSerializer(serializers.HyperlinkedModelSerializer):
    versions = serializers.RelatedField(many=True)

    class Meta:
        model = Engine


class ServerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Server


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Project


class ProjectTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProjectType


class EngineVersionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EngineVersion


class ExtensionVersionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ExtensionVersion


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    # versions = serializers.

    class Meta:
        model = Category


class ExtensionSerializer(serializers.HyperlinkedModelSerializer):
    versions = serializers.SerializerMethodField('get_versions')
    category_info = serializers.SerializerMethodField('get_category_info')

    class Meta:
        model = Extension

    def get_versions(self, obj):
        qs = obj.versions.all()
        result = [{
            'version': q.version,
            'file': q.file.url,
            'changelog': q.changelog,
        } for q in qs]
        return result

    def get_category_info(self, obj):
        return {
            'name': obj.category.name,
            'slug': obj.category.slug,
            'order': obj.category.order,
        }


# class ExtensionVersionSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = ExtensionVersion
