# coding: utf-8
from __future__ import unicode_literals
import requests

from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.response import Response
from django.core.exceptions import PermissionDenied

from projects.models import Project, Server, Engine, ProjectType,\
    Version as EngineVersion
from repository.models import Category, Extension, Version as ExtensionVersion
from api.serializers import ProjectSerializer, ServerSerializer,\
    ProjectTypeSerializer, EngineSerializer, \
    EngineVersionSerializer, ExtensionVersionSerializer, \
    CategorySerializer, ExtensionSerializer

from projects.utils import extension_list_url


@api_view(['GET'])
def api_root(request, format=None):
    """
    The entry endpoint of our API.
    """
    return Response({
        # 'projects': reverse('project-list', request=request),
        'extensions': reverse('extension-list', request=request),
    })


class ProjectList(generics.ListAPIView):
    """
    API endpoint that represents a list of projects.
    """
    model = Project
    serializer_class = ProjectSerializer


class ProjectDetail(generics.RetrieveAPIView):
    """
    API endpoint that represents a single project.
    """
    model = Project
    serializer_class = ProjectSerializer


class ServerList(generics.ListAPIView):
    """
    API endpoint that represents a list of servers.
    """
    model = Server
    serializer_class = ServerSerializer


class ServerDetail(generics.RetrieveAPIView):
    """
    API endpoint that represents a single server.
    """
    model = Server
    serializer_class = ServerSerializer


class EngineList(generics.ListAPIView):
    """
    API endpoint that represents a list of engines.
    """
    model = Engine
    serializer_class = EngineSerializer


class EngineDetail(generics.RetrieveAPIView):
    """
    API endpoint that represents a single engine.
    """
    model = Engine
    serializer_class = EngineSerializer


class ProjectTypeList(generics.ListAPIView):
    """
    API endpoint that represents a list of project types.
    """
    model = ProjectType
    serializer_class = ProjectTypeSerializer


class ProjectTypeDetail(generics.RetrieveAPIView):
    """
    API endpoint that represents a single project type.
    """
    model = ProjectType
    serializer_class = ProjectTypeSerializer


class EngineVersionList(generics.ListAPIView):
    """
    API endpoint that represents a list of engine versions.
    """
    model = EngineVersion
    serializer_class = EngineVersionSerializer


class EngineVersionDetail(generics.RetrieveAPIView):
    """
    API endpoint that represents a single engine version.
    """
    model = EngineVersion
    serializer_class = EngineVersionSerializer

# ----------------------


class CategoryList(generics.ListAPIView):
    """
    API endpoint that represents a list of categories.
    """
    model = Category
    serializer_class = CategorySerializer


class CategoryDetail(generics.RetrieveAPIView):
    """
    API endpoint that represents a single category.
    """
    model = Category
    serializer_class = CategorySerializer


class ExtensionList(generics.ListAPIView):
    """
    API endpoint that represents a list of extensions.
    """
    model = Extension
    serializer_class = ExtensionSerializer

    def get_queryset(self):
        if self.request.method == 'GET':
            from itertools import chain
            project_slug = self.request.GET.get('project_name', '')
            secret_key = self.request.GET.get('secret_key', '')
            try:
                project = Project.objects.get(
                    slug=project_slug,
                    secret_key=secret_key)
            except Project.DoesNotExist:
                raise PermissionDenied

            extensions1 = project.engine.service_extensions.filter(
                versions__isnull=False,
            )
            ids = [e.id for e in extensions1]
            extensions2 = project.extensions.filter(
                versions__isnull=False,
            ).exclude(id__in=ids).distinct()
            return chain(extensions1, extensions2)
        else:
            raise PermissionDenied


class ExtensionDetail(generics.RetrieveAPIView):
    """
    API endpoint that represents a single extension.
    """
    model = Extension
    serializer_class = ExtensionSerializer


class ExtensionVersionList(generics.ListAPIView):
    """
    API endpoint that represents a list of extension versions.
    """
    model = ExtensionVersion
    serializer_class = ExtensionVersionSerializer


class ExtensionVersionDetail(generics.RetrieveAPIView):
    """
    API endpoint that represents a single extension version.
    """
    model = ExtensionVersion
    serializer_class = ExtensionVersionSerializer


@api_view(['POST'])
def update_engine(request, format=None):
    res = {'result': 'Success. Engine version updated'}
    try:
        engine_slug = request.POST.get('engine_slug', '')
        version = request.POST.get('version', '')
        changelog = request.POST.get('changelog', '')
        file = request.FILES['file']

        engine = Engine.objects.get(slug=engine_slug)
        if EngineVersion.objects.filter(version=version).count() == 0:
            EngineVersion.objects.create(
                engine=engine,
                version=version,
                file=file,
                changelog=changelog
            )
        else:
            version = EngineVersion.objects.get(
                engine=engine,
                version=version,
            )
            version.file = file
            if changelog != '':
                version.changelog = changelog
            version.save()
    except Exception, e:
        res = {
            'result': str(e)
        }

    return Response(res)


@api_view(['POST'])
def update_plugin(request, format=None):
    res = {'result': 'Success. Plugin updated on sitepanel'}
    try:
        plugin_slug = request.POST.get('plugin_slug', '')
        version = request.POST.get('version', '')
        changelog = request.POST.get('changelog', '')
        # username = request.POST.get('username', '')
        _file = request.FILES['file']

        extension = Extension.objects.get(slug=plugin_slug)
        if ExtensionVersion.objects.filter(
                version=version, extension=extension).count() == 0:
            ExtensionVersion.objects.create(
                extension=extension,
                version=version,
                file=_file,
                changelog=changelog
            )
        else:
            version = ExtensionVersion.objects.get(
                extension=extension,
                version=version,
            )
            version.file = _file
            if changelog != '':
                version.changelog = changelog
            version.save()
    except Exception, e:
        res = {
            'result': str(e)
        }

    return Response(res)


@api_view(['POST'])
def update_plugin_on_projects(request, format=None):
    res = {}
    try:
        failed = []
        error = ''

        plugin_slug = request.POST.get('plugin_slug', '')
        version = request.POST.get('version', '')
        user = request.POST.get('user', 'sitepanel')
        update = request.POST.get('update', '').split(',')
        update_all = eval(request.POST.get('update-all', 'False'))

        if update_all is False:
            projects = Project.objects.filter(
                slug__in=update,
                extensions__slug=plugin_slug
            ).exclude(server__name="localhost")
        else:
            projects = Project.objects.filter(
                extensions__slug=plugin_slug
            ).exclude(server__name="localhost")
        for project in projects:
            data = {
                'project_name': project.slug,
                'secret_key': project.secret_key,
                'slug': plugin_slug,
                'version': version,
                'user': user,
            }
            url = 'http://%s/sbits-plugins-api/update-extension/'
            r = requests.post(url % project.domain, data=data)
            try:
                if eval(r.text)['result'] != 'success':
                    failed.append(project.slug)
            except Exception, e:
                failed.append('%s - %s' % (project.slug, str(e)))
        if failed:
            error = '. Возникли проблемы на: %s' % ';'.join(failed)
            res['result'] = "Расширения обновлены.%s" % error
        else:
            res['result'] = "Расширения обновлены: %s" %\
                ', '.join([a['slug'] for a in projects.values('slug')])
    except Exception, e:
        res = {
            'result': str(e)
        }

    return Response(res)


@api_view(['POST'])
def get_project_values(request, format=None):
    res = {}
    try:
        project_slug = request.POST.get('project_slug', '')
        project = Project.objects.filter(
            slug=project_slug,
        )
        if project.count() == 0:
            res = {
                'result': 'No project with slug: %s' % project_slug
            }
        else:
            project = project[0]
            url = '%sapi/get-project-info/' % project.server.manager_url
            data = {
                'project_slug': project_slug,
                'secret_key': project.server.sign
            }
            r = requests.post(url, data=data)
            res = r.json()

    except Exception, e:
        res = {
            'result': str(e)
        }

    return Response(res)


@api_view(['POST'])
def get_project_env(request, format=None):
    res = {}
    try:
        project_slug = request.POST.get('project_slug', '')
        last = eval(request.POST.get('last', False))
        project = Project.objects.filter(
            slug=project_slug,
        )
        if project.count() == 0:
            res = {
                'result': 'No project with slug: %s' % project_slug
            }
        else:
            project = project[0]
            if last:
                urls = []
                for x in project.extensions.all():
                    url = x.versions.all().order_by('-pk')[0].file.url
                    urls.append(url)

                result = 'pip install %s' % ' '.join(urls)
                res['result'] = result

            else:
                link = extension_list_url.format(
                    domain=project.domain,
                    secret_key=project.secret_key,
                    slug=project.slug)
                r = requests.get(link)
                if r.status_code == 200:
                    urls = []
                    data = r.json()
                    versions = {}
                    for x in data['extension_list']:
                        if x['installed_version']:
                            versions[x['slug']] = x['installed_version']

                    exts = Extension.objects.filter(slug__in=versions.keys())
                    for ext in exts:
                        try:
                            url = ext.versions.get(
                                version=versions[ext.slug]).file.url
                            urls.append(url)
                        except:  # for testing on local machine
                            pass

                    result = 'pip install %s' % ' '.join(urls)
                    res['result'] = result
                else:
                    res = r.json()

    except Exception, e:
        res = {
            'result': str(e)
        }

    return Response(res)
