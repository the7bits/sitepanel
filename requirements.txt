Django==1.5.1
Fabric==1.4.3
Markdown==2.3.1
Pillow==2.0.0
PyYAML==3.10
South==0.7.6
argparse==1.2.1
#distribute==0.6.24
django-activeurl==0.1.4
django-bootstrap-toolkit==2.10.4
django-classy-tags==0.4
django-filter==0.6
-e git://github.com/derek-schaefer/django-json-field.git@51d17120199455c088ebe7ad51c34c62a486b403#egg=django_json_field-dev
django-mptt==0.5.5
django-polymorphic==0.5
django-reversion==1.7
-e git+https://github.com/darklow/django-suit@d99a7684bd6bf8d4da26fed7f0389ae67da86a71#egg=django_suit-dev
django-suit-ckeditor==0.0.2
djangorestframework==2.2.6
docutils==0.10
easy-thumbnails==1.2
jsonfield==0.9.5
lxml==3.1.1
psycopg2==2.5
python-dateutil==2.1
requests==1.2.0
six==1.3.0
wsgiref==0.1.2
